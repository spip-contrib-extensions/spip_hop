<?php

/**
 * Déclaration systématiquement chargées.
 **/

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// ********
// Permettre de surcharger la taille pour le petit écran et le LARGEUR écran
// Important : la valeur renseignée doit avoir une unité de mesure : em, rem, px, %, pt.
// exemple : 1200px
// ********

if (!defined('_LARGEUR_ECRAN')) {
	define('_LARGEUR_ECRAN', '');
}
